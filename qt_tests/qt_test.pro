QT += testlib widgets
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app
INCLUDEPATH += ../inc ../utils ../src ..\src\SortingController

SOURCES += ../src/SortingController/QuicksortSortingControllerImpl.cpp \
    ../src/SortingController/BubbleSortingControllerImpl.cpp \
    test_main.cpp
SOURCES +=  \
    tst_basictest.hpp \
    sorting_controller_test.hpp \

HEADERS += ../src/SortingController/QuicksortSortingControllerImpl.hpp \
../src/SortingController/SortingControllerImpl.hpp \
../src/SortingController/BubbleSortingControllerImpl.h

# This is a bit funky... But if we want to test the class
# we need to build it in the project.

SOURCES +=  ../utils/RandomNumGenerator.cpp
