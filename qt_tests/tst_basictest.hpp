#pragma once
#include <QtTest>
#include <RandomNumGenerator.hpp>

class basicTest : public QObject
{
    Q_OBJECT

  public:
    basicTest();
    ~basicTest();

  private slots:
    void random_generator_preduces_different_numbers_each_time();
    void generator_inst_is_returing_the_same_inst_eahc_time();
};

basicTest::basicTest()
{
}

basicTest::~basicTest()
{
}

void basicTest::generator_inst_is_returing_the_same_inst_eahc_time()
{
    auto &inst1 = utils::RandomNumGenerator::get_inst();
    auto &inst2 = utils::RandomNumGenerator::get_inst();

    QCOMPARE(&inst1, &inst2);
}
void basicTest::random_generator_preduces_different_numbers_each_time()
{
    auto &generator = utils::RandomNumGenerator::get_inst();
    uint32_t value1 = generator.generate(0, 10000);
    uint32_t value2 = generator.generate(0, 10000);

    // Well, technically, it's not always true,
    // if we get a true random distribution.
    // We'll worry about that bridge once we get to it.
    // Or more likely ignore it.
    QVERIFY(value1 != value2);
}

#include "tst_basictest.moc"
