#include "sorting_controller_test.hpp"
#include "tst_basictest.hpp"

int main(int argc, char *argv[])
{
    // setup lambda
    int status = 0;
    auto runTest = [&status, argc, argv](QObject *obj) { status |= QTest::qExec(obj, argc, argv); };

    runTest(new basicTest);
    runTest(new QuicksortSortingControllerTest);
    runTest(new BubbleSortingControllerTest);
    return status;
}
