#pragma once
#include <../src/SortingController/BubbleSortingControllerImpl.h>
#include <../src/SortingController/QuicksortSortingControllerImpl.hpp>
#include <QtTest>
#include <algorithm>
#include <iostream>

std::vector<uint32_t> sort_cpp(std::vector<uint32_t> vec)
{
    std::sort(vec.begin(), vec.end());
    return vec;
};

class QuicksortSortingControllerTest : public sorting::QuicksortSortingControllerImpl
{
    Q_OBJECT
  public:
    QuicksortSortingControllerTest();
    ~QuicksortSortingControllerTest();

  private:
    std::vector<uint32_t> sort_and_return_data(std::vector<uint32_t> vec)
    {
        this->perform_sorting(vec.begin(), vec.end());
        return vec;
    };

  private slots:
    void test_sort();
};

QuicksortSortingControllerTest::QuicksortSortingControllerTest() : sorting::QuicksortSortingControllerImpl{}
{
}

QuicksortSortingControllerTest::~QuicksortSortingControllerTest()
{
}

void QuicksortSortingControllerTest::test_sort()
{
    std::vector<std::vector<uint32_t>> data = {
        {1}, {1, 2}, {2, 1}, {1, 2, 3}, {2, 3, 1}, {1, 4, 3, 2},
    };

    for (const auto &dataset : data)
    {
        QCOMPARE(sort_and_return_data(dataset), sort_cpp(dataset));
    }
}

class BubbleSortingControllerTest : public sorting::BubbleSortingControllerImpl
{
    Q_OBJECT
  public:
    BubbleSortingControllerTest();
    ~BubbleSortingControllerTest();

  private:
    std::vector<uint32_t> sort_and_return_data(std::vector<uint32_t> vec)
    {
        this->perform_sorting(vec.begin(), vec.end());
        return vec;
    };

  private slots:
    void test_sort();
};

BubbleSortingControllerTest::BubbleSortingControllerTest() : sorting::BubbleSortingControllerImpl{}
{
}

BubbleSortingControllerTest::~BubbleSortingControllerTest()
{
}

inline void BubbleSortingControllerTest::test_sort()
{
    std::vector<std::vector<uint32_t>> data = {
        {1}, {1, 2}, {2, 1}, {1, 2, 3}, {2, 3, 1}, {1, 4, 3, 2},
    };

    for (const auto &dataset : data)
    {
        QCOMPARE(sort_and_return_data(dataset), sort_cpp(dataset));
    }
}

#include "sorting_controller_test.moc"
