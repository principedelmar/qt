#include "SortingChartView.hpp"
#include "RandomNumGenerator.hpp"
#include <iostream>
#include <thread>

namespace sorting::view
{

SortingChartView::SortingChartView(QWidget *parent, const QString &chart_name)
    : QChartView{parent}, name_of_chart{chart_name}
{
    chart_ptr = new QChart();
    QChartView::setChart(chart_ptr);
    QChartView::setRenderHint(QPainter::Antialiasing);
    QChartView::setViewportUpdateMode(QGraphicsView::ViewportUpdateMode::FullViewportUpdate);
    QChartView::setFixedSize(500, 900);
}

SortingChartView::~SortingChartView()
{
}

void SortingChartView::update(const std::vector<uint32_t> &data)
{
    clear_chart();
    set_data(data);
    update_chart();
}

void SortingChartView::clear_chart()
{
    chart_ptr->removeAllSeries();
}

void SortingChartView::update_chart()
{
    QBarSeries *bar_series_ptr = create_new_series_from_data();

    chart_ptr->addSeries(bar_series_ptr);
    chart_ptr->setTitle(name_of_chart);
    chart_ptr->setAnimationOptions(QChart::NoAnimation);
}

void SortingChartView::set_data(const std::vector<uint32_t> &data)
{
    this->data = data;
}

QBarSeries *SortingChartView::create_new_series_from_data()
{
    QBarSeries *bar_series_ptr = new QBarSeries();
    bar_series_ptr->append(create_new_set_from_data());
    return bar_series_ptr;
}

QBarSet *SortingChartView::create_new_set_from_data()
{
    QBarSet *bar_set_ptr = new QBarSet("Data to sort");
    for (uint32_t i = 0; i < data.size(); ++i)
        *bar_set_ptr << data[i];
    return bar_set_ptr;
}

} // namespace sorting::view
