#include "SortingController.hpp"

namespace sorting
{

void SortingController::sort(std::vector<uint32_t> vec)
{
    emit send_sort_to_impl(vec);
}

void SortingController::connect_to_impl()
{

    impl_ptr->moveToThread(&controller_thread);

    connect(&controller_thread, &QThread::finished, impl_ptr, &QObject::deleteLater);

    connect(impl_ptr, SIGNAL(visualize_data(std::vector<uint32_t>)), this,
            SIGNAL(visualize_data(std::vector<uint32_t>)));
    connect(this, SIGNAL(send_sort_to_impl(std::vector<uint32_t>)), impl_ptr, SLOT(sort(std::vector<uint32_t>)));

    controller_thread.start();
}

SortingController::SortingController(SortingControllerImpl *impl_ptr) : QObject{}, impl_ptr{impl_ptr}
{
    connect_to_impl();
}

SortingController::~SortingController()
{
    controller_thread.quit();
    controller_thread.wait();
    delete impl_ptr;
}

} // namespace sorting
