#include "MainWindow.h"
#include <QApplication>
#include <QPushButton>
#include <SortingChartView.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow window{};
    window.show();
    return a.exec();
}
