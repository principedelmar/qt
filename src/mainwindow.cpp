#include "MainWindow.h"
#include <QPushButton>
#include <RandomNumGenerator.hpp>
#include <algorithm>
#include <iostream>
#include <src/SortingController/SortingControllerImplFactory.hpp>

using namespace sorting;

void MainWindow::create_button()
{
    m_button = new QPushButton("Hello World", this);
}

void MainWindow::add_new_view_controller_pair()
{
    if (view_controller_pairs.size() >= 2)
        return;
    auto *view = new view::SortingChartView{this};
    view->move(view_controller_pairs.size() * 500, 0);
    auto *controller = new SortingController{QuicksortControllerImplFactory::inst().create()};
    QPair<view::SortingChartView *, SortingController *> pair{view, controller};
    view_controller_pairs.push_back(pair);

    connect(this, SIGNAL(update_data(std::vector<uint32_t>)), view, SLOT(update(std::vector<uint32_t>)));
    connect(controller, SIGNAL(visualize_data(std::vector<uint32_t>)), view, SLOT(update(std::vector<uint32_t>)));
    connect(this, SIGNAL(begin_sorting(std::vector<uint32_t>)), controller, SLOT(sort(std::vector<uint32_t>)));
}

void MainWindow::add_new_view_bubble_controller_pair()
{
    if (view_controller_pairs.size() >= 2)
        return;
    auto *view = new view::SortingChartView{this};
    view->move(view_controller_pairs.size() * 500, 0);
    auto *controller = new SortingController{BubbleControllerImplFactory::inst().create()};
    QPair<view::SortingChartView *, SortingController *> pair{view, controller};
    view_controller_pairs.push_back(pair);

    connect(this, SIGNAL(update_data(std::vector<uint32_t>)), view, SLOT(update(std::vector<uint32_t>)));
    connect(controller, SIGNAL(visualize_data(std::vector<uint32_t>)), view, SLOT(update(std::vector<uint32_t>)));
    connect(this, SIGNAL(begin_sorting(std::vector<uint32_t>)), controller, SLOT(sort(std::vector<uint32_t>)));
}

MainWindow::MainWindow(QWidget *parent) : QWidget{parent}
{
    setFixedSize(1000, 1000);
    create_button();
    m_button->setCheckable(true);
    m_button->move(500, 900);
    connect(m_button, SIGNAL(clicked(bool)), this, SLOT(begin_sorting(bool)));
    add_new_view_controller_pair();
    add_new_view_bubble_controller_pair();
}

MainWindow::~MainWindow()
{
}

void MainWindow::begin_sorting(bool checked)
{
    if (checked)
    {
        std::vector<uint32_t> data(25);
        std::generate(data.begin(), data.end(),
                      []() { return utils::RandomNumGenerator::get_inst().generate(1, 100); });
        emit update_data(data);
        emit begin_sorting(data);
    }
}
