#ifndef QUICKSORTSORTINGCONTROLLERIMPL_HPP
#define QUICKSORTSORTINGCONTROLLERIMPL_HPP

#include "SortingControllerImpl.hpp"
#include <QObject>
#include <QThread>

namespace sorting
{

class QuicksortSortingControllerImpl : public SortingControllerImpl
{
    Q_OBJECT
  public:
    QuicksortSortingControllerImpl();
    virtual ~QuicksortSortingControllerImpl() override;

  protected:
    void perform_sorting(std::vector<uint32_t>::iterator begin, std::vector<uint32_t>::iterator end) override;
    template <class ForwardIt, class UnaryPredicate>
    ForwardIt partition(ForwardIt first, ForwardIt last, UnaryPredicate p)
    {
        first = std::find_if_not(first, last, p);
        if (first == last)
            return first;

        for (ForwardIt i = std::next(first); i != last; ++i)
        {
            if (p(*i))
            {
                std::iter_swap(i, first);
                QThread::usleep(50);
                ++first;
            }
        }
        return first;
    }
};

} // namespace sorting

#endif // QUICKSORTSORTINGCONTROLLERIMPL_HPP
