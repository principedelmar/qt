#ifndef BUBBLESORTINGCONTROLLERIMPL_H
#define BUBBLESORTINGCONTROLLERIMPL_H

#include "SortingControllerImpl.hpp"
#include <QObject>

namespace sorting
{

class BubbleSortingControllerImpl : public SortingControllerImpl
{
  public:
    BubbleSortingControllerImpl();
    virtual ~BubbleSortingControllerImpl() override;

  protected:
    void perform_sorting(std::vector<uint32_t>::iterator begin, std::vector<uint32_t>::iterator end) override;
};

} // namespace sorting
#endif // BUBBLESORTINGCONTROLLERIMPL_H
