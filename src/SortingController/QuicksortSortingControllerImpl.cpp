#include "QuicksortSortingControllerImpl.hpp"
#include <algorithm>

namespace sorting
{

QuicksortSortingControllerImpl::QuicksortSortingControllerImpl()
{
}

QuicksortSortingControllerImpl::~QuicksortSortingControllerImpl()
{
}

void QuicksortSortingControllerImpl::perform_sorting(std::vector<uint32_t>::iterator begin,
                                                     std::vector<uint32_t>::iterator end)
{
    if (begin == end)
        return;
    auto pivot = *std::next(begin, std::distance(begin, end) / 2);
    std::vector<uint32_t>::iterator middle1 = partition(begin, end, [pivot](const auto &em) { return em < pivot; });
    std::vector<uint32_t>::iterator middle2 =
        partition(middle1, end, [pivot](const auto &em) { return !(pivot < em); });

    emit visualize_data(data);

    perform_sorting(begin, middle1);
    perform_sorting(middle2, end);
}

} // namespace sorting
