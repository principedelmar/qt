#include "BubbleSortingControllerImpl.h"
#include <QThread>
#include <algorithm>

namespace sorting
{

BubbleSortingControllerImpl::BubbleSortingControllerImpl()
{
}

BubbleSortingControllerImpl::~BubbleSortingControllerImpl()
{
}

void BubbleSortingControllerImpl::perform_sorting(std::vector<uint32_t>::iterator begin,
                                                  std::vector<uint32_t>::iterator end)
{
    while (1)
    {
        bool was_swapped = false;
        for (auto iter = begin; iter != end; iter++)
        {
            auto next = iter + 1;

            if (next != end && *iter > *next)
            {
                std::swap(*next, *iter);
                QThread::usleep(50);
                was_swapped = true;
            }
        }
        if (!was_swapped)
            return;

        emit visualize_data(data);
    }
}

} // namespace sorting
