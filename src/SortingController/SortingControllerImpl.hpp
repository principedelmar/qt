#ifndef SORTINGCONTROLLERIMPL_HPP
#define SORTINGCONTROLLERIMPL_HPP

#include <QObject>
#include <iostream>

namespace sorting
{

class SortingControllerImpl : public QObject
{
    Q_OBJECT
  public:
    SortingControllerImpl();
    ~SortingControllerImpl() override;
  signals:
    void visualize_data(std::vector<uint32_t> vec);
  public slots:
    virtual void sort(std::vector<uint32_t> vec);

  protected:
    virtual void perform_sorting(std::vector<uint32_t>::iterator begin, std::vector<uint32_t>::iterator end);

    std::vector<uint32_t> data;
    std::atomic<bool> sorting_in_progress;
};

inline void SortingControllerImpl::sort(std::vector<uint32_t> vec)
{
    if (!sorting_in_progress)
    {
        data = vec;
        sorting_in_progress = true;
        perform_sorting(data.begin(), data.end());
        sorting_in_progress = false;
    }
}

inline void SortingControllerImpl::perform_sorting(std::vector<uint32_t>::iterator begin,
                                                   std::vector<uint32_t>::iterator end)
{
    Q_UNUSED(begin);
    Q_UNUSED(end);
    std::cerr << "SortingControllerImpl::perform_sorting() called, this should not happen\n";
}

inline SortingControllerImpl::SortingControllerImpl() : QObject{}, data{}, sorting_in_progress{false}
{
}

inline SortingControllerImpl::~SortingControllerImpl()
{
}

} // namespace sorting

#endif // SORTINGCONTROLLERIMPL_HPP
