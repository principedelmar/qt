#ifndef SORTINGCONTROLLERIMPLFACTORY_HPP
#define SORTINGCONTROLLERIMPLFACTORY_HPP

#include "SortingControllerImpl.hpp"
#include <memory>

namespace sorting
{

class SortingControllerImplFactory
{
  public:
    virtual SortingControllerImpl *create() = 0;

  protected:
    SortingControllerImplFactory() = default;
};

class QuicksortControllerImplFactory : SortingControllerImplFactory
{
  protected:
    QuicksortControllerImplFactory() = default;

  public:
    SortingControllerImpl *create() override;
    static SortingControllerImplFactory &inst();
};

class BubbleControllerImplFactory : SortingControllerImplFactory
{
  protected:
    BubbleControllerImplFactory() = default;

  public:
    SortingControllerImpl *create() override;
    static SortingControllerImplFactory &inst();
};

} // namespace sorting

#endif // SORTINGCONTROLLERIMPLFACTORY_HPP
