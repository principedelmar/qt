#include "SortingControllerImplFactory.hpp"
#include "BubbleSortingControllerImpl.h"
#include "QuicksortSortingControllerImpl.hpp"

namespace sorting
{

SortingControllerImpl *QuicksortControllerImplFactory::create()
{
    return new QuicksortSortingControllerImpl;
}

QuicksortControllerImplFactory::SortingControllerImplFactory &QuicksortControllerImplFactory::inst()
{
    static QuicksortControllerImplFactory factory;
    return factory;
}

SortingControllerImpl *BubbleControllerImplFactory::create()
{
    return new BubbleSortingControllerImpl;
}

SortingControllerImplFactory &BubbleControllerImplFactory::inst()
{
    static BubbleControllerImplFactory factory;
    return factory;
}

} // namespace sorting
