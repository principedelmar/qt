#ifndef RANDOMNUMGENERATOR_HPP
#define RANDOMNUMGENERATOR_HPP

#include <cstdint>

namespace utils
{

class RandomNumGenerator
{
  public:
    static RandomNumGenerator &get_inst();
    uint32_t generate(uint32_t min, uint32_t max);

  private:
    RandomNumGenerator() = default;
};

} // namespace utils

#endif // RANDOMNUMGENERATOR_HPP
