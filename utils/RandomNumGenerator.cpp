#include "RandomNumGenerator.hpp"
#include <random>

namespace utils
{
uint32_t RandomNumGenerator::generate(uint32_t min, uint32_t max)
{
    std::mt19937 generator{std::random_device{}()};
    std::uniform_int_distribution<int> distribution(min, max);
    return distribution(generator);
}

RandomNumGenerator &RandomNumGenerator::get_inst()
{
    static RandomNumGenerator generator;
    return generator;
}

} // namespace utils
