QT += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += inc utils

SOURCES += \
    src/MainWindow.cpp \
    src/SortingController/QuicksortSortingControllerImpl.cpp \
    src/SortingChartView.cpp \
    src/SortingController.cpp \
    src/SortingController/SortingControllerImplFactory.cpp \
    src/SortingController/BubbleSortingControllerImpl.cpp \
    src/main.cpp \
    utils/RandomNumGenerator.cpp

HEADERS += \
    inc/MainWindow.h \
    inc/SortingChartView.hpp \
    inc/SortingController.hpp \
    src/SortingController/QuicksortSortingControllerImpl.hpp \
    src/SortingController/BubbleSortingControllerImpl.h \
    src/SortingController/SortingControllerImpl.hpp \
    src/SortingController/SortingControllerImplFactory.hpp \
    utils/RandomNumGenerator.hpp

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
