#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QPair>
#include <QVector>
#include <QWidget>

#include "SortingChartView.hpp"
#include "SortingController.hpp"

class MainWindow : public QWidget
{
    Q_OBJECT
  public:
    explicit MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow() override;

  private:
    void create_button();
    void add_new_view_controller_pair();
    void add_new_view_bubble_controller_pair();
    QVector<QPair<sorting::view::SortingChartView *, sorting::SortingController *>> view_controller_pairs;
    QPushButton *m_button;
  private slots:
    void begin_sorting(bool checked);
  signals:
    void update_data(std::vector<uint32_t> data);
    void begin_sorting(std::vector<uint32_t> data);
};

#endif // MAINWINDOW_H
