#ifndef SORTINGCONTROLLER_HPP
#define SORTINGCONTROLLER_HPP

#include <QObject>
#include <QThread>
#include <src/SortingController/SortingControllerImpl.hpp>

namespace sorting
{

class SortingController : public QObject
{
    Q_OBJECT
  public:
    SortingController(SortingControllerImpl *impl_ptr);
    ~SortingController() override;

  protected:
    void perform_sorting(std::vector<uint32_t>::iterator begin, std::vector<uint32_t>::iterator end);

  private:
    void connect_view_to_impl_signals_and_slots();
    void connect_to_impl();
    SortingControllerImpl *impl_ptr;
    QThread controller_thread;
  signals:
    void visualize_data(std::vector<uint32_t> vec);
    void send_sort_to_impl(std::vector<uint32_t> vec);
  public slots:
    void sort(std::vector<uint32_t> vec);
};

} // namespace sorting

#endif // SORTINGCONTROLLER_HPP
