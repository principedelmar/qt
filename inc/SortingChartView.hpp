#ifndef SORTINGCHARTVIEW_H
#define SORTINGCHARTVIEW_H

#include <QWidget>
#include <QtCharts>
#include <cstdint>
#include <string>
#include <vector>

namespace sorting::view
{

class SortingChartView : public QChartView
{
    Q_OBJECT
  public:
    SortingChartView(QWidget *parent, const QString &chart_name = "quicksort data");
    virtual ~SortingChartView() override;
  public slots:
    void update(const std::vector<uint32_t> &data);

  private:
    void set_data(const std::vector<uint32_t> &data);
    void clear_chart();
    void update_chart();
    QBarSet *create_new_set_from_data();
    QBarSeries *create_new_series_from_data();

    QString name_of_chart;
    std::vector<uint32_t> data;
    QChart *chart_ptr;
};

} // namespace sorting::view

#endif // SORTINGCHARTVIEW_H
